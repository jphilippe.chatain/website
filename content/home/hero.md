+++
# Hero widget.
widget = "hero"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"

  # Background gradient.
  gradient_start = "#4bb4e3"
  gradient_end = "#2b94c3"

  # Text color (true=light or false=dark).
  text_color_light = true

[cta]
  url = "mailto:jphilippe.chatain@gmail.com"
  label = "Get In Touch"
+++

I can help you make the difference between a good codebase and a great product.
