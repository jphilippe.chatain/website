+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Software Architect"
  company = "SoftBank Robotics America"
  company_url = "https://www.softbankrobotics.com/us/"
  location = "California"
  date_start = "2018-12-01"
  description = """
Pepper - Accompanied product refinement from internal MVP to public launch, dockerized the solution to reduce new developer 
on-boarding (git clone to running) from 1 day to 1 hour

Whiz - Contributed to new web portal product for analytics from first business requirement to internal launch in under 3 
months, owned the API management layer built on Apigee (multi-tenancy, RBAC, centralized fault handling)

Cloud DevOps Platform - Contributed to the design concept of "core services and extensions", advocated for adoption throughout 
the organization  
  """

[[experience]]
  title = "Software Engineer"
  company = "SoftBank Robotics America"
  company_url = "https://www.softbankrobotics.com/us/"
  location = "California"
  date_start = "2017-12-01"
  date_end = "2018-12-01"
  description = """
Contributed to a new product from ideation to internal MVP with a cross-functional team of 10 engineers and designers

Advocated for early generalization, helping to seamlessly change target verticals twice

Managed the insourcing of a contractor’s 5 year old legacy codebase

Coached team members and stakeholders, tying back together design & development concerns and business needs
"""

[[experience]]
  title = "Solutions Engineer"
  company = "SoftBank Robotics America"
  company_url = "https://www.softbankrobotics.com/us/"
  location = "California"
  date_start = "2016-04-01"
  date_end = "2017-12-01"
  description = """
- Development of robotic application used in production, most demos and all interviews as an introduction to Pepper.

- Technical support for 20+ demos & tech discussions with prospects, 60+ press/marketing events/interviews/... and all of POCs 
/ pop-ups.

- Directly contributed to the generation of over $2.8M of ad value through content development, technical support during 
interviews and expos, taking part in interviews.

- Started the training program for developer Partners in the US: 15 Partners trained.

- Directly dispensed 10 trainings leading to Partner status and robots acquisitions, or key in POCs' success.

- Addressed over 100+ enquiries from various Partners through tickets/emails/calls.
"""

[[experience]]
  title = "Training Officer"
  company = "SoftBank Robotics Europe"
  company_url = "https://www.softbankrobotics.com/emea/en/index"
  location = "Paris, France"
  date_start = "2014-12-01"
  date_end = "2016-04-01"
  description = """
Demonstration / Pre-sales (B2B, B2A)

Application development and voice interface

NAO/Pepper fleet management

External developers community training
"""

+++
