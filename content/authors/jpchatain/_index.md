---
# Display name
title: Jean-Philippe Chatain

# Username (this should match the folder name)
authors:
- jpchatain

# Is this the primary user of the site?
superuser: true

# Role/position
role: Software Architect, Products & Tools builder

# Short bio (displayed in user profile at end of posts)
bio: I'm a software professional perpetually looking for the next way to help and the next thing to learn.

interests:
- Product development
- DevOps / developer tooling
- Automation
- "Space colonization :rocket:"

education:
  courses:
  - course: MSc in Computer Science
    institution: Staffordshire University, UK
    year: 2014
  - course: MEng in Mechanical, Electrical and Industrial Engineering
    institution: Ecole catholique d'Arts et Métiers de Lyon, France
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:jphilippe.chatain@gmail.com
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jpchatain
- icon: linkedin
  icon_pack: fab
  link: https://linkedin.com/in/jpchatainsoftwareengineer
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/jphilippe.chatain

---

I relish seeing people enjoy their now-free time when their tools got just a little bit better, easier, more automated.
